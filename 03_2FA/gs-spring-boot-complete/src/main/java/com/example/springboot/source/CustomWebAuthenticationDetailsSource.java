import javax.servlet.http.HttpServletRequest;

@Component
	public class CustomWebAuthenticationDetailsSource implements 
	  AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> {
	    
	    @Override
	    public WebAuthenticationDetails buildDetails(HttpServletRequest context) {
	        return new CustomWebAuthenticationDetailsSource();
	    }
	}