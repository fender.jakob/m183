package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.data.User;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
	Logger log = LoggerFactory.getLogger(HelloController.class);

	@RequestMapping("/")
	public String index(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String twofa = request.getParameter("2fa");
		boolean isLoggedIn = false;

		List<User> userList = new ArrayList<>();
		String generatedCode = Base32.random();
		log.info(generatedCode);
		userList.add(new User("user1", "pwd123", "25K7VMSNQ2AN7RHG"));
		userList.add(new User("user2", "pwd321", generatedCode));

		User selectedUser = userList.stream()
				.filter(u -> u.getUsername().equals(username))
				.findFirst()
				.orElse(null);

		if (selectedUser != null && selectedUser.getPassword().equals(password)) {
            Totp totp = new Totp(selectedUser.getTwofaCode());
            isLoggedIn = totp.verify(twofa);

		}

		if (isLoggedIn) {
			return "you are logged in";
		} else {
			return "<form>" + "username: <input type='text' name='username'/><br/>"
					+ "password: <input type='text' name='password'/><br/>"
					+ "2fa: <input type='text' name='2fa'/><br/>" + "<button type='submit'> senden </button>"
					+ "</form>";
		}
	}

}
