/**
 * This message gets called when the authentication process with google succeeded.
 *
 * @param googleUser
 */
function onSuccess(googleUser) {
    console.log('Logged in as: ' + googleUser.getBasicProfile().getName());

    // TODO: Assign the id token as value to this constant.
    const idToken = googleUser.getAuthResponse().id_token;

    const requestBody = JSON.stringify({
        "idToken": idToken
    });

    // TODO: Send an http POST request to the verification endpoint to validate the id token.
    // In case you'll receive an 415 http status: Consider to set the Content-Type request header to 'application/json'.
    // This page might be helpful: https://developers.google.com/identity/sign-in/web/backend-auth
    const verificationEndpoint = '/User/VerifyIdToken';

     fetch(verificationEndpoint, {
        method: "POST",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application / json'
        },
        body: requestBody
     }).then(response => response.json()).then(data => {
         if (data.statusCode !== 400) {
            window.location.replace('/Profile');
         }
     });
    // When the validation was successful, the user is fully authenticated.

}