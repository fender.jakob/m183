function signOut() {
    // Enable the auth2 api.
    gapi.load('auth2', function () {
        gapi.auth2.init()
            .then(function () {
                const auth2 = gapi.auth2.getAuthInstance();

                auth2.signOut().then(function () {
                    toggleLogoutState();
                })
            });
    });
}

/**
 * Toggles the visibility of two opposite paragraphes in the frontend to indicated the current status of the sing-out process.
 */
function toggleLogoutState() {
    document.getElementById('logout-pending').style.display = 'none';
    document.getElementById('logout-complete').style.display = 'block';
}

// When this file is loaded by the browser, the signOut function will be called.
signOut();

