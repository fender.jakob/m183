# M183

## 02_Keylogger
In 02 ist ein Keylogger, der die Tastenanschläge mit Sonderzeichen speichert und an https://m183.gibz-informatik.ch/api/keylogger sendet.
Hier wurde eine Map für die Values der Tastenanschläge verwendet. Dies kann man verbessern.

## 03_2FA
In 03 wurde eine 2FA Applikation geschrieben. Unter `03_2FA/gs-spring-boot-complete/src/main/java/com/example/springboot/HelloController.java`
ist die ganze Applikation in einen Controller geschustert. Dies kann am verschönert werden.

## 04_ Clickjacking
In 04 wurde die gibz.zg.ch Seite nachgestellt. Die Seite sendet die eingegebenen Credentials an https://m183.gibz-informatik.ch/api/uiredresscredential


## 06_SSO
Funktioniert nicht

## 07_HTTP Digest
Funktioniert nicht

## 08_HTTP Digest
Funktioniert nicht
